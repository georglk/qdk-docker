# Docker image for QDK

Create Docker image for QNAP Development Kit.

## QDK

https://github.com/qnap-dev/QDK

### Download QDK

https://github.com/qnap-dev/QDK/releases/latest

## Build

Сборка docker образа.

### Use GitLab CI/CD with Docker to create Docker images

https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

### Local build

Сборка образа и запуск контейнера `qdk-docker`:

```shell
cd <path_to_repo>
docker build . -f Dockerfile -t qdk-docker && docker run --rm -i -t --name qdk-docker qdk-docker
```

## Links

https://github.com/qnap-dev/QDK-Guide

https://github.com/qeek-dev/create-dpkg

https://github.com/qnap-dev/containerized-qpkg

https://github.com/rednoah/docker-qdk
