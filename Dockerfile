# https://docs.docker.com/engine/reference/builder/

FROM ubuntu:20.04

RUN DEBIAN_FRONTEND=noninteractive \
apt-get update && \
apt-get install --no-install-recommends -y dos2unix && \
rm -rf /var/lib/apt/lists/*

WORKDIR /opt/QDK
COPY QDK .

RUN find . -type f -print0 | xargs -0 dos2unix

RUN ./InstallToUbuntu.sh install

ENV PATH=$PATH:/usr/share/QDK/bin
